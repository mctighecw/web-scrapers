# README

A project to try out web scraping with Python.

## App Information

App Name: web-scrapers

Created: July 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/web-scrapers)

## Tech Stack

- Python (3.6)
- Beautiful Soup 4
- Scrapy
- SQLAlchemy
- PostgreSQL

## To Run

1. Set up `venv`
2. Install Python requirements
3. Set up Postgres database and initialize
4. Run scraping scripts (in `/src`)

Last updated: 2024-08-01
