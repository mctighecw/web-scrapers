import os
import requests

def get_working_dir():
    print(f'Working directory: {os.getcwd()}')


def check_robots_file(url):
    req_response = requests.get(f'{url}/robots.txt', timeout=5)

    if req_response.status_code == 200:
        bytes_response = req_response.content
        str_response = str(bytes_response, 'utf-8')
        permissions = { 'Disallowed': [], 'Allowed': [] }

        for line in str_response.split('\n'):
            if line.startswith('Allow'):
                permissions['Allowed'].append(line.split(': ')[1].split(' ')[0])
            elif line.startswith('Disallow'):
                permissions['Disallowed'].append(line.split(': ')[1].split(' ')[0])

        print(permissions)
        return permissions

    else:
        if req_response.status_code == 404:
            print('No robots.txt file found, so scraping is by default allowed')

        else:
            err_res = req_response.status_code
            print(f'Error code: {err_res}')

        return { 'Disallowed': [], 'Allowed': ['/'] }


def remove_quotes(string):
    res = string.replace('“', '').replace('”', '')
    return res
