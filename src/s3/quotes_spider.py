# See https://docs.scrapy.org/en/latest/intro/tutorial.html

import sys
from pathlib import Path
import scrapy

src_dir = Path(__file__).parent.parent
sys.path.append(str(src_dir))

from misc.helpers import remove_quotes
from database.models import Quote, Author
from database.db import get_new_session


class QuotesSpider(scrapy.Spider):
    """
    Scrape url and collect both quotes and authors info.
    Parsing based solely on CSS selectors. Data is saved
    to the 'quotes' and 'authors' tables in Postgres.
    """

    name = 'quotes_spider'
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        """
        Default callback method that parses the Scrapy response
        """
        # get quotes data
        quotes_data = []

        for q in response.css('div.quote'):
            quotes_data.append({
                'text': q.css('span.text::text').get(),
                'author': q.css('span small::text').get(),
                'tags': q.css('div.tags a.tag::text').getall(),
            })

        # add to database
        try:
            session = get_new_session()

            for q in quotes_data:
                text = remove_quotes(q['text'])
                author = q['author']
                tags = ''

                for t in q['tags']:
                    tags += f'{t}, '

                db_data = Quote(text=text, author=author, tags=tags)
                session.add(db_data)

            session.commit()

        except:
            print('Error saving to database')


        # links to author pages
        for href in response.css('.author + a::attr(href)'):
            yield response.follow(href, self.parse_author)

        # subsequent page links
        for a in response.css('li.next a'):
            yield response.follow(a, self.parse)


    def parse_author(self, response):
        """
        Custom method to parse author data
        """

        def extract_with_css(query):
            return response.css(query).get(default='').strip()

        authors_data = []

        authors_data.append({
            'name': extract_with_css('h3.author-title::text'),
            'birthday': extract_with_css('.author-born-date::text'),
            'bio': extract_with_css('.author-description::text'),
        })

        # add to database
        try:
            session = get_new_session()

            for a in authors_data:
                name = a['name']
                birthday = a['birthday']
                bio = a['bio']

                db_data = Author(name=name, birthday=birthday, bio=bio)
                session.add(db_data)

            session.commit()

        except:
            print('Error saving to database')
