import sys
from pathlib import Path
import scrapy

src_dir = Path(__file__).parent.parent
sys.path.append(str(src_dir))

from misc.constants import star_table
from database.models import Book
from database.db import get_new_session


class BooksSpider(scrapy.Spider):
    """
    Scrape url and collect books info (title, price, star rating).
    Parsing based on XPath expressions. Data is saved to the 'books'
    table in Postgres.
    """

    name = 'books_spider'
    start_urls = ['http://books.toscrape.com/']

    def parse(self, response):
        """
        Default callback method that parses the Scrapy response
        """

        titles = response.xpath('//article/h3/a/@title').getall()
        prices = response.xpath('//article/div/p[contains(@class, "price_color")]/text()').getall()
        star_ratings = response.xpath('//article/p[contains(@class, "star-rating")]/@class').getall()

        stars = []
        book_data = []

        for s in star_ratings:
            # remove 'star-rating '
            t = s[12:]
            stars.append(star_table[t])

        for t, p, s in zip(titles, prices, stars):
            book_data.append({
                'title': t,
                'price': p,
                'stars': s
            })

        # add to database
        try:
            session = get_new_session()

            for b in book_data:
                title = b['title']
                price = b['price']
                stars = b['stars']

                db_data = Book(title=title, price=price, stars=stars)
                session.add(db_data)

            session.commit()

        except:
            print('Error saving to database')

        # subsequent page links
        for href in response.xpath('//ul/li[contains(@class, "next")]/a/@href'):
            yield response.follow(href, self.parse)
