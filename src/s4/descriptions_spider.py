import sys
from pathlib import Path
import scrapy

src_dir = Path(__file__).parent.parent
sys.path.append(str(src_dir))

from database.models import Description
from database.db import get_new_session


class DescriptionsSpider(scrapy.Spider):
    """
    Scrape url and collect book descriptions.
    Parsing based on XPath expressions. Data is saved
    to the 'descriptions' table in Postgres.
    """

    name = 'descriptions_spider'
    start_urls = ['http://books.toscrape.com/']

    def parse(self, response):
        """
        Default callback method that parses the Scrapy response
        """

        links = response.xpath('//article/div[contains(@class, "image_container")]/a/@href').getall()

        # get description for each book
        for link in links:
            yield response.follow(link, self.get_description)

        # subsequent page links
        for href in response.xpath('//ul/li[contains(@class, "next")]/a/@href'):
            yield response.follow(href, self.parse)

    def get_description(self, response):
        """
        Custom method to get and parse description data
        """

        title = response.xpath('//h1/text()').get()
        description = response.xpath('//head/meta[contains(@name, "description")]/@content').get()

        # add to database
        try:
            session = get_new_session()
            db_data = Description(title=title, description=description)
            session.add(db_data)
            session.commit()

        except:
            print('Error saving to database')
