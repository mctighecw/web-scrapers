import re
from bs4 import BeautifulSoup
import requests

from misc.helpers import check_robots_file
from misc.constants import headers
from configs.bs_config import parser_options, timeout

from database.models import ScrapedData
from database.db import get_new_session

def scrape_url(url, path='/'):
    print(f'Scraping {url}{path}')

    permissions = check_robots_file(url)
    scraping_allowed = False

    if path in permissions['Allowed'] or path not in permissions['Disallowed']:
        scraping_allowed = True

    if (scraping_allowed):
        try:
            # page_response = requests.get(url, timeout=timeout, headers=headers)
            page_response = requests.get(url, timeout=timeout)
            parser = parser_options[0]

            if page_response.status_code == 200:
                soup = BeautifulSoup(page_response.content, parser)

                # get book topics
                nav_list = soup.find('ul', { 'class': 'nav-list' })
                nav_list_a = nav_list.find_all('a')

                ignore_strings = ['Books', 'Add a comment']
                book_topics = []

                for t in nav_list_a:
                    t = t.get_text().strip()

                    if t not in ignore_strings:
                        book_topics.append(t)

                # get book titles
                # only get 'a href' elements with 'catalogue' not followed by 'category' or 'page'
                book_titles_a = soup.find_all('a', {'href' : re.compile(r'catalogue/(?!category|page/*)')})
                book_titles = []

                for t in book_titles_a:
                    t = t.get_text()

                    if len(t) > 0:
                        book_titles.append(t)

                # add to database
                try:
                    url = f'{url}{path}'
                    data = { 'book_topics': book_topics, 'book_titles': book_titles }

                    db_data = ScrapedData(url=url, data=data)
                    session = get_new_session()
                    session.add(db_data)
                    session.commit()

                except:
                    print('Error saving to database')

            else:
                error_code = page_response.status_code
                print(f'Error code: {error_code}')

        except requests.Timeout as err:
            error_res = str(err)
            print(f'Timeout occurred: {error_res}')
