import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, JSON, DateTime

Base = declarative_base()


class ScrapedData(Base):
    """
    Table for scraped data using Beautiful Soup
    """
    __tablename__ = 'scraped_data'

    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(String)
    data = Column(JSON)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)


class Quote(Base):
    """
    Table for quotes data using Scrapy
    """
    __tablename__ = 'quotes'

    id = Column(Integer, primary_key=True, autoincrement=True)
    text = Column(String)
    author = Column(String)
    tags = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)


class Author(Base):
    """
    Table for quotes' authors data using Scrapy
    """
    __tablename__ = 'authors'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    birthday = Column(String)
    bio = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)


class Book(Base):
    """
    Table for books data using Scrapy
    """
    __tablename__ = 'books'

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    stars = Column(Integer)
    price = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)


class Description(Base):
    """
    Table for book descriptions using Scrapy
    """
    __tablename__ = 'descriptions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    description = Column(String, default='')
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
