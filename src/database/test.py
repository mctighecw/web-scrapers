from database.models import ScrapedData
from database.db import get_new_session

def test_db():
    try:
        test_data = ScrapedData(url='http://www.test.com/', data={'foo':'bar'})
        session = get_new_session()
        session.add(test_data)
        session.commit()
        print('test_db: okay')

    except:
        print('test_db: error')
