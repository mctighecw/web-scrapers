import sys
from pathlib import Path
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

src_dir = Path(__file__).parent.parent
sys.path.append(str(src_dir))

from configs.env_config import DB_USER, DB_PASSWORD, DB_HOST, DB_NAME

engine = create_engine(f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}', echo=False)

def get_new_session():
    db_session = scoped_session(sessionmaker(bind=engine))
    session = db_session()
    return session
