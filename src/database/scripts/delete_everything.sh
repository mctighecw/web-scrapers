#!/usr/bin/env bash

echo Deleting web-scrapers database and user...

ADMIN_USER=$USER
source ../../../.env

echo Closing any open db connections...
sudo -u $ADMIN_USER psql postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$DB_NAME' AND pid <> pg_backend_pid();"

echo Deleting old db...
sudo -u $ADMIN_USER dropdb $DB_NAME

echo Deleting old user...
sudo -u $ADMIN_USER dropuser $DB_USER
