#!/usr/bin/env bash

echo Creating new web-scrapers database...

ADMIN_USER=$USER
source ../../../.env

if [ "$( psql $ADMIN_USER -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DB_USER'" )" != '1' ]
then
  echo Creating user: ws_user. Please enter password "\"$DB_PASSWORD"\"
  sudo -u $ADMIN_USER createuser -P $DB_USER
  sudo -u $ADMIN_USER createdb -O $DB_USER -E 'utf8' -T 'template0' $DB_NAME
fi

echo Closing any open db connections...
sudo -u $ADMIN_USER psql postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$DB_NAME' AND pid <> pg_backend_pid();"

echo Deleting old db...
sudo -u $ADMIN_USER dropdb $DB_NAME

echo Creating new db...
sudo -u $ADMIN_USER createdb -O $DB_USER -E 'utf8' -T 'template0' $DB_NAME
