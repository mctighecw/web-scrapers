from bs4 import BeautifulSoup
import requests

from misc.helpers import check_robots_file, remove_quotes
from misc.constants import headers
from configs.bs_config import parser_options, timeout

from database.models import ScrapedData
from database.db import get_new_session

def scrape_url(url, path='/'):
    print(f'Scraping {url}{path}')

    permissions = check_robots_file(url)
    scraping_allowed = False

    if path in permissions['Allowed'] or path not in permissions['Disallowed']:
        scraping_allowed = True

    if (scraping_allowed):
        try:
            # page_response = requests.get(url, timeout=timeout, headers=headers)
            page_response = requests.get(url, timeout=timeout)
            parser = parser_options[0]

            if page_response.status_code == 200:
                soup = BeautifulSoup(page_response.content, parser)

                quote_texts = soup.find_all('span', class_='text')
                quote_authors = soup.find_all('small', itemprop='author')

                quotes_dict = {}
                i = 0

                for a, t in zip(quote_authors, quote_texts):
                    index = str(i)
                    a = a.get_text()
                    t = t.get_text()
                    t = remove_quotes(t)

                    quotes_dict[index] = { 'author': a, 'text': t }
                    i += 1

                # add to database
                try:
                    url = f'{url}{path}'
                    data = { 'quotes': quotes_dict }

                    db_data = ScrapedData(url=url, data=data)
                    session = get_new_session()
                    session.add(db_data)
                    session.commit()

                except:
                    print('Error saving to database')

            else:
                error_code = page_response.status_code
                print(f'Error code: {error_code}')

        except requests.Timeout as err:
            error_res = str(err)
            print(f'Timeout occurred: {error_res}')
